import { NextFunction, Request, Response, ErrorRequestHandler } from 'express';
import { HttpException } from "#/utils/exceptions/http.exception";
import { HTTP_STATUS_CODES } from '#/utils/constants/status-codes.constant';


export function errorMiddleware(error: HttpException, request: Request, response: Response, next: NextFunction) {
  const status = error.status || HTTP_STATUS_CODES.INTERNAL_SERVER_ERROR;
  const message = error.message || 'Something went wrong';
  return response
    .status(status)
    .send({
      message,
      status,
    });
}