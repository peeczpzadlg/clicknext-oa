module.exports = {
    entry: './src/main.ts',
    output: {
      filename: 'bundle.js',
     path: path.resolve(__dirname, 'dist')
    },
    resolve: {
      extensions: ['.ts', '.js', '.mjs']
    },
    module: {
      rules: [
        {
          test: /.ts$/,
          use: 'ts-loader',
          exclude: /node_modules/
        }
      ]
    },
    experiments: {
      outputModule: true
    }
  };