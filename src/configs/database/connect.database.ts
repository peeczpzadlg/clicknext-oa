
import * as dotenv from "dotenv";
import mongoose from 'mongoose';
import bluebird from 'bluebird';

export const connectDatabase = async () => {
    mongoose.Promise = bluebird;
    let mogodbURI = process.env.MONGODB_URI;
    // if(process.env.NODE_ENV === "test"){
    //   mogodbURI = process.env.MONGODB_URI_LOCAL;
    // }
    // if(process.env.isDevelopment) mongoose.set('debug', true);

    return await mongoose.connect("mongodb://localhost:27017/?retryWrites=true&serverSelectionTimeoutMS=5000&connectTimeoutMS=10000&3t.uriVersion=3&3t.connection.name=Clicknext-OA&3t.alwaysShowAuthDB=true&3t.alwaysShowDBFromUserRole=true", {
      // useNewUrlParser: true,
      // useCreateIndex: true,
      // useUnifiedTopology: true,
      // useFindAndModify: false,
    })
    .then((result) => {
      console.log('Mongo connected!! DB:', result.connection.db.databaseName);
    })
    .catch(err => {
      console.log(`MongoDB connection error. Please make sure MongoDB is running. ${err}`);
      process.exit();
    });
  }