import express from "express";
import { cleanEnv, port, str } from "envalid";
import * as dotenv from "dotenv";
import * as bodyParser from 'body-parser';
import * as fs from "fs";
import { IController } from "#/utils/types/index";
import { connectDatabase } from "#/configs/database/connect.database";
import { errorMiddleware } from "#/middleware/handler.middleware";
import fileUpload from "express-fileupload";
import { TestingController } from "#/controllers/test.controller";

class App {

    private app = express();

    constructor(controllers: IController[]) {
      // this.app = express();
      this.validateEnv();
      this.initializeControllers(controllers);
      // connectDatabase()
      // this.connectDatabase()
      // .then(()=>{
      //   this.initializeMiddlewares();
      //   this.initializeControllers(controllers);
      //   this.initializeErrorHandling();
      // });
    }

    public listen() {
        this.app.listen(process.env.PORT, () => {
            console.log(`App listening on the port ${process.env.PORT}`);
        });
    }

    public getServer() {
        return this.app;
    }

    private initializeControllers(controllers: IController[]) {
      controllers.forEach((controller) => {
        this.app.use('/api/v1'+controller.path, controller.router);
      });
      if(process.env.isTest) return;
      console.info('NODE_ENV', process.env.NODE_ENV);

    }

    private initializeMiddlewares() {
      this.app.use(bodyParser.urlencoded({ extended: true }));
      this.app.use(bodyParser.json({limit: '4MB'}));
      // this.app.use(Auth.initialize());
      this.app.use(
        fileUpload({
          limits: { fileSize: 4 * 1024 * 1024 },
          useTempFiles: true,
          // tempFileDir : '/tmrouterp/'
        })
        );
    }
  

    private initializeErrorHandling()  {
      this.app.use(errorMiddleware);
    }

    private validateEnv() {
      if (fs.existsSync(".env")) {
        console.log("Using .env file to supply config environment variables");
        dotenv.config({ path: ".env" });
      } else {
        dotenv.config({ path: ".env.example" });  // you can delete this after you create your own .env file!
        console.log("Using .env.example file to supply config environment variables");
      }
      cleanEnv(process.env, {
        NODE_ENV: str(),
        // JWT_SECRET: str(),
        PORT: port(),
      });
      // if(process.env.isProduction || process.env.isDevelopment){
      //   cleanEnv(process.env, {
      //     MONGODB_URI: str(),
      //   });
      // }else{
      //   cleanEnv(process.env, {
      //     MONGODB_URI_LOCAL: str(),
      //   });
      // }

    }

}

// const app = new App([TestingController]);
// app.listen();
export default App;
// export { app, App };