import { IController } from "#/utils/types/index";
import { Router, Request, Response, NextFunction } from "express";


class TestController implements IController {
    path: string;
	router: Router;

	constructor() {
		this.path = '/test';
		this.router = Router();
        this.initRouter();
	}

    initRouter() {
        this.router.get("/fe", this.testingLocal);
    }

    private async testingLocal(req: Request, res: Response, next: NextFunction) {
        return res.status(200).send({message: "Data Successfully"});
    }
}

export const TestingController = new TestController();
export const TestingRouter = TestingController.router;