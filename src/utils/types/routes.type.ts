import { Router, Request } from 'express';

export interface IController {
    path: string;
    router: Router;
}