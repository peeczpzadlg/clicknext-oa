
import App from '#/app';
import { TestingController } from '#/controllers/test.controller';

const app = new App(
    [
      TestingController
    ],
  );
  
app.listen();